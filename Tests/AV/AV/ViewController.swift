//
//  ViewController.swift
//  AV
//
//  Created by Kc on 06/03/2016.
//  Copyright © 2016 Kenechi Okolo. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    // MARK: - Properties & Variables
    let stillImageOutput = AVCaptureStillImageOutput()
    var session: AVCaptureSession!
    var frontCamera: AVCaptureDevice!
    var player: AVCaptureVideoPreviewLayer!
    
    // MARK: - IB Outlets & Actions
    @IBOutlet weak var captureView: UIView!
    @IBAction func captureImage(sender: UIButton) {
        if let videoConnection = stillImageOutput.connectionWithMediaType(AVMediaTypeVideo) {
            stillImageOutput.captureStillImageAsynchronouslyFromConnection(videoConnection) {
                (imageDataSampleBuffer, error) -> Void in
                let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer)
                UIImageWriteToSavedPhotosAlbum(UIImage(data: imageData)!, nil, nil, nil)
                print("Image captured")
            }
        }
    }
    
    // MARK: - View Setup & Implementation
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        player.frame = view.bounds
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        session = AVCaptureSession()
        session.sessionPreset = AVCaptureSessionPresetHigh
        
        frontCamera = getFrontCamera()
        
        var input: AVCaptureDeviceInput!
        
        do {
            try input = AVCaptureDeviceInput(device: frontCamera)
            if session.canAddInput(input) {
                session.addInput(input)
                player = AVCaptureVideoPreviewLayer(session: session)
                captureView.layer.addSublayer(player)
                
                session.startRunning()
            }
            if session.canAddOutput(stillImageOutput) {
                stillImageOutput.outputSettings = [AVVideoCodecKey:AVVideoCodecJPEG]
                session.addOutput(stillImageOutput)
            }
        } catch let inputError {
            print("Error getting input: \(inputError)")
        }
        
    }
    
    // MARK: - Helper Functions
    override func shouldAutorotate() -> Bool {
        return false
    }
    func getFrontCamera() -> AVCaptureDevice? {
        let availableCaptureDevices = AVCaptureDevice.devicesWithMediaType(AVMediaTypeVideo)
        var camera: AVCaptureDevice?
        for device in availableCaptureDevices {
            if device.position == .Front {
                camera = device as? AVCaptureDevice
            }
        }
        return camera
    }
}

