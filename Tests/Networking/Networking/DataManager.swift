//
//  DataManager.swift
//  Networking
//
//  Created by Kc on 05/03/2016.
//  Copyright © 2016 Kenechi Okolo. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import CoreData

class DataManager {
    
    var taskList = [JSON]()
    
    func getRemoteData() {
        Alamofire.request(.GET, "https://bitbucket.org/fatunicorn/iostest/raw/78f902ddb31ac96bb23b901054442dee52664271/data/tasklist.json").responseJSON { response in
            if let value = response.result.value {
                let json = JSON(value)
                self.saveJSONObject(json)
                NSNotificationCenter.defaultCenter().postNotificationName("dataRetrieved", object: nil)
            }
        }
    }
    func saveJSONObject(object: JSON) {
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let entity = NSEntityDescription.entityForName("Task", inManagedObjectContext: managedContext)
        
        for item in object.arrayValue {
            let task = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
            task.setValue(item["done"].intValue, forKey: "done")
            task.setValue(item["image"].stringValue, forKey: "image")
            task.setValue(item["task"].stringValue, forKey: "task")
            
            taskList.append(item)
        }
        
        do {
            try managedContext.save()
            print("ManagedContext saved")
        } catch let error as NSError {
            print("CoreData Error: \(error). Could not save: \(managedContext)")
        }
    }
}