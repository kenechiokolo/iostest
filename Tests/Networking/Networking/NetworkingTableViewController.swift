//
//  NetworkingTableViewController.swift
//  Networking
//
//  Created by Kc on 05/03/2016.
//  Copyright © 2016 Kenechi Okolo. All rights reserved.
//

import UIKit
import SwiftyJSON

class NetworkingTableViewController: UITableViewController {
    
    // MARK: - Properties & Variables
    
    let dataManager = DataManager()
    var model = [JSON]()
    var completedTasks = [JSON]()
    var uncompletedTasks = [JSON]()

    // MARK: - Segmented Control
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBAction func filterTasks() {
        tableView.reloadData()
    }

    // MARK: - Helper Functions
    
    func updateModel() {
        model = dataManager.taskList
        for task in model {
            if task["done"].boolValue {
                completedTasks.append(task)
            } else {
                uncompletedTasks.append(task)
            }
        }
        tableView.reloadData()
    }

    // MARK: - Superclass Override
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "updateModel", name: "dataRetrieved", object: nil)
        
        dataManager.getRemoteData()
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch segmentedControl.selectedSegmentIndex {
        case 0: return uncompletedTasks.count
        case 1: return completedTasks.count
        case 2: return model.count
        default:
            return 0
        }
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("taskCell", forIndexPath: indexPath)
        
        var data = model
        
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            data = uncompletedTasks
        case 1:
            data = completedTasks
        default:
            break
        }

        cell.textLabel?.text = data[indexPath.row]["task"].stringValue

        return cell
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }

}
