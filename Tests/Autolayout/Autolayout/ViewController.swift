//
//  ViewController.swift
//  Autolayout
//
//  Created by Kc on 06/03/2016.
//  Copyright © 2016 Kenechi Okolo. All rights reserved.
//
/*  The upper text field and UIButton are anchored to the lower text field.
    The lower text field has its Y position constrained to match the superview's center Y and
    thus moving the lower text field moves all the boxes accordingly.
*/

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var upperTextField: UITextField!
    @IBOutlet weak var lowerTextField: UITextField!
    @IBOutlet weak var buttonOutlet: UIButton!
    @IBOutlet weak var yConstraint: NSLayoutConstraint!
    
    let center = NSNotificationCenter.defaultCenter()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        center.addObserver(self, selector: "adjustLayout:", name: UIKeyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: "restoreLayout", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    
    func adjustLayout(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
        let buttonBottomEdge = buttonOutlet.frame.origin.y + buttonOutlet.frame.height
        
        if keyboardFrame.origin.y < buttonBottomEdge {
            UIView.animateWithDuration(0.5, animations:  { Void in
                self.yConstraint.constant = (keyboardFrame.origin.y - buttonBottomEdge)
            })
        }
    }
    
    func restoreLayout() {
        yConstraint.constant = 0
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        center.removeObserver(self)
    }


}

